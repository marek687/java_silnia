

public class Main {

    public Main()
    {
        System.out.println("Działa konstruktor klasy Main");

    }
    private static long getFactorial(final long digit)
    {
        if(digit <1) {
            return 1;
        }
        else
        {
            return digit * getFactorial(digit-1);
        }
    }
    public static void main(String[] args) {

        long digit=4;

        Main main=new Main();
        long factorial=main.getFactorial(digit);

        System.out.printf("Silnia(%d) = %d \n",digit,factorial);

    }
}
